
import Route from './router'

;(function (d, c) {

  const App = {
    init () {
      c('module App is running...')
      this.cacheDOM()
      this.bindListeners()
    },
    onMouseMove (e) {
      this.$mosaicContainer.style.left = `calc(50% - ${e.clientX * 0.2}px)`
      this.$mosaicContainer.style.top = `calc(33% - ${e.clientY * 0.05}px)`
    },
    toggleCalendar () {
      this.$aside.classList.toggle('opened')
      this.$modalContainer.classList.toggle('opened')
    },
    cacheDOM () {
      this.$mosaicContainer = document.querySelector('.mosaic-container')
      this.$calendarToggle = document.querySelector('.calendar-icon')
      this.$aside = document.querySelector('aside')
      this.$modalContainer = document.querySelector('.modal-container')
    },
    bindListeners () {
      window.addEventListener('mousemove', this.onMouseMove.bind(this))
      this.$calendarToggle.addEventListener('click', this.toggleCalendar.bind(this))
      this.$modalContainer.addEventListener('click', this.toggleCalendar.bind(this))
    }
  }

  Route('/').use(App)

})( document, console.log )
