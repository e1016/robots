
import Route from './router'

;(function (d, c) {

  const Sponsor = {
    init () {
      c('module Sponsor is running...')
      this.cacheDOM()
      this.bindListeners()
    },
    cacheDOM () {

    },
    bindListeners () {
      
    }
  }

  Route('/patrocina').use(Sponsor)

})( document, console.log )
