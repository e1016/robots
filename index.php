<?php

if (isset( $_GET['route'])) {
	switch ($_GET['route']) {

  	case 'cotiza':       require('public/quote.php');           break;
  	case 'patrocina':    require('public/sponsors.php');        break;
  	case 'donar':        require('public/donate.php');          break;

    case 'participar':   require('public/participate/participate.php');     break;

    /*
    * Specific rules
    */
    case 'tochito': {
      require('public/donate.php');
    } break;
    case 'sumo': {
      require('public/donate.php');
    } break;
    case 'other': {
      require('public/donate.php');
    } break;

    /*
    * default 404 response
    */
  	default:
  		header("HTTP/1.0 404 Not Found");
  		require_once('public/404.php');
  	break;
	}
} else {
	require_once('public/home.php');
}

?>
