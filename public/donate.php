<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Donar | Batalla de robots BCS</title>
  <?php require('public/base/meta.php'); ?>
</head>
<body class="donate">
  <header>
    <?php require('public/base/header.php') ?>
  </header>
  <main>
    <div class="resp-iframe">
      <iframe src="https://www.youtube.com/embed/1lOikKyGHfs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </iframe>
    <div>
      <a href="#">
        <div class="blue-btn">DONAR</div>
      </a>
    </div>
  </main>
</body>
<?php require('public/base/footer.php'); ?>
</html>
