<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Batalla de robots BCS</title>
  <?php require('public/base/meta.php'); ?>
</head>
<body class="main">
  <div class="modal-container"></div>
  <header>
    <a href="/" class="logo">
      <img src="/R/logo.png" width="200" alt="Logo de Batalla de Robots La Paz BCS">
    </a>
    <nav>
      <ul>
        <li class="menu_item"><a href="/participar">Participar</a></li>
        <li class="menu_item"><a href="/patrocina">Patrocina</a></li>
        <li class="menu_item"><a href="/stand">Stand</a></li>
        <li class="menu_item"><a href="/asistir">Asistir</a></li>
        <li class="menu_item"><a href="/donar">Donar</a></li>
      </ul>
    </nav>
    <img class="calendar-icon" src="/R/img/calendar.svg" width="32" alt="Ícono de calendario">
  </header>
  <main>
    <section class="main-section">
      <div class="mosaic-container">
        <div class="mosaic-row">
          <?php for ($i = 0; $i < 8; $i++) { ?>
            <div class="mosaic-icons"></div>
          <?php } ?>
        </div>
        <div class="mosaic-row" style="transform:translate(-133px);margin-top:-133px;">
          <?php for ($i = 0; $i < 8; $i++) { ?>
            <?php if ($i == 3) { ?>
              <a href="/tochito"><div class="mosaic-icons">Tochito kids</div></a>
            <?php } else if ($i == 4) { ?>
              <a href="/sumo"><div class="mosaic-icons">Sumo</div></a>
            <?php } else if ($i == 5) { ?>
              <a href="/others"><div class="mosaic-icons">Semi</div></a>
            <?php } else if ($i == 6) { ?>
              <a href="/others"><div class="mosaic-icons">Pro</div></a>
            <?php } else {?>
              <div class="mosaic-icons">&nbsp;</div>
            <?php } ?>
          <?php } ?>
        </div>
        <div class="mosaic-row" style="margin-top:-134px;">
          <?php for ($i = 0; $i < 8; $i++) { ?>
            <div class="mosaic-icons"></div>
          <?php } ?>
        </div>
        <div class="mosaic-row" style="transform:translate(-133px);margin-top:-133px;">
          <?php for ($i = 0; $i < 8; $i++) { ?>
            <div class="mosaic-icons"></div>
          <?php } ?>
        </div>
        <div class="mosaic-row" style="margin-top:-134px;">
          <?php for ($i = 0; $i < 8; $i++) { ?>
            <div class="mosaic-icons"></div>
          <?php } ?>
        </div>
      </div>
    </section>
    <aside>
      <h2>Calendario</h2>
      <?php for ($i = 0; $i < 20; $i++) { ?>
        <div class="date-cont">
          <div class="left">
            <div class="day-letter">viernes</div>
            <div class="day-number">25</div>
            <div class="day-month">enero</div>
          </div>
          <div class="right">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
          </div>
        </div>
      <?php } ?>
    </aside>
  </main>
  <?php require('public/base/footer.php'); ?>
</body>
</html>
