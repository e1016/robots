<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Participar | Batalla de robots BCS</title>
    <?php require('public/base/meta.php'); ?>
  </head>
  <body class="participate">
    <header>
      <?php require('public/base/header.php') ?>
    </header>
    <main>
      <h1>Participar</h1>
      <ul>
        <li>
          <div class="image"></div>
          <div class="content">
            <h2>Sumo</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <div class="anchors">
            <a href="#">Formato de inscripción <img src="/R/img/arrow-up-right.svg" width="16"></a>
            <a href="#">Reglamento <img src="/R/img/arrow-up-right.svg" width="16"></a>
            <a href="#">Formato de responsividad <img src="/R/img/arrow-up-right.svg" width="16"></a>
            <br>
            <br>
            <a href="#">Más información <img src="/R/img/arrow-right.svg" width="16"></a>
          </div>
        </li>
        <li>
          <div class="image"></div>
          <div class="content">
            <h2>Sumo</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <div class="anchors">
            <a href="#">Formato de inscripción <img src="/R/img/arrow-up-right.svg" width="16"></a>
            <a href="#">Reglamento <img src="/R/img/arrow-up-right.svg" width="16"></a>
            <a href="#">Formato de responsividad <img src="/R/img/arrow-up-right.svg" width="16"></a>
            <br>
            <br>
            <a href="#">Más información <img src="/R/img/arrow-right.svg" width="16"></a>
          </div>
        </li>
        <li>
          <div class="image"></div>
          <div class="content">
            <h2>Sumo</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <div class="anchors">
            <a href="#">Formato de inscripción <img src="/R/img/arrow-up-right.svg" width="16"></a>
            <a href="#">Reglamento <img src="/R/img/arrow-up-right.svg" width="16"></a>
            <a href="#">Formato de responsividad <img src="/R/img/arrow-up-right.svg" width="16"></a>
            <br>
            <br>
            <a href="#">Más información <img src="/R/img/arrow-right.svg" width="16"></a>
          </div>
        </li>
        <li>
          <div class="image"></div>
          <div class="content">
            <h2>Sumo</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <div class="anchors">
            <a href="#">Formato de inscripción <img src="/R/img/arrow-up-right.svg" width="16"></a>
            <a href="#">Reglamento <img src="/R/img/arrow-up-right.svg" width="16"></a>
            <a href="#">Formato de responsividad <img src="/R/img/arrow-up-right.svg" width="16"></a>
            <br>
            <br>
            <a href="#">Más información <img src="/R/img/arrow-right.svg" width="16"></a>
          </div>
        </li>
      </ul>
    </main>
  <?php require('public/base/footer.php'); ?>
  </body>
</html>
