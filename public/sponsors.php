<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Patrocinio | Batalla de robots BCS</title>
  <?php require('public/base/meta.php'); ?>
</head>
<body class="sponsors">
  <header>
    <?php require('public/base/header.php') ?>
  </header>
  <main>
    <h1>Patrocinio</h1>
    <div class="resp-iframe">
      <iframe src="https://www.youtube.com/embed/1lOikKyGHfs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </iframe>
  </div>

  <div class="table">
    <ul>
      <li><b>Tarifa</b></li>
      <li><b>Beneficios</b></li>
      <li><b>Acciones</b></li>
    </ul>
    <ul>
      <li>$500.00</li>
      <li>Beneficios del paquete</li>
      <li><a href="#">PAGAR</a></li>
    </ul>
  </div>

</main>
</body>
<?php require('public/base/footer.php'); ?>
</html>
