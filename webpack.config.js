const path = require('path');

module.exports = {
  entry: './R/js/src/index.js',
  output: {
    path: path.resolve(__dirname, 'R/js/'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  watch: true
};
